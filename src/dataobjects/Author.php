<?php

namespace Hestec\BlogExtensions;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Blog\Model\Blog;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Security\Permission;

class Author extends DataObject {

    private static $singular_name = 'Author';
    private static $plural_name = 'Authors';

    private static $table_name = 'HestecBlogExtensionsAuthor';

    private static $db = [
        'FirstName' => 'Varchar(100)',
        'Surname' => 'Varchar(100)',
        'Bio' => 'HTMLText',
        'Linkedin' => 'Varchar(255)',
        'Twitter' => 'Varchar(255)',
        'Facebook' => 'Varchar(255)',
        'Instagram' => 'Varchar(255)',
        'Bluesky' => 'Varchar(255)',
        'Mastodon' => 'Varchar(255)'
    ];

    private static $has_one = [
        'Blog' => Blog::class,
        'ProfileImage' => Image::class
    ];

    private static $has_many = [
        'BlogPosts' => BlogPost::class
    ];

    private static $owns = array(
        'ProfileImage'
    );

    private static $summary_fields = [
        'FirstName',
        'Surname'
    ];

    public function getCMSFields() {
        //$fields = parent::getCMSFields();

        $FirstNameField = TextField::create('FirstName', 'FirstName');
        $SurnameField = TextField::create('Surname', 'Surname');
        $BioField = HTMLEditorField::create('Bio', 'Bio');
        $BioField->setRows(5);
        $LinkedinField = TextField::create('Linkedin', 'Linkedin');
        $TwitterField = TextField::create('Twitter', 'Twitter');
        $FacebookField = TextField::create('Facebook', 'Facebook');
        $InstagramField = TextField::create('Instagram', 'Instagram');
        $BlueskyField = TextField::create('Bluesky', 'Bluesky');
        $MastodonField = TextField::create('Mastodon', 'Mastodon');

        $ProfileImageField = new UploadField('ProfileImage', 'ProfileImage');
        $ProfileImageField->allowedExtensions = array('jpg', 'png', 'gif');
        //$ImageField->setDescription("Beste afmeting: 300 x 300.");
        $ProfileImageField->setFolderName("ProfileImage");


        return new FieldList(
            $FirstNameField,
            $SurnameField,
            $BioField,
            $ProfileImageField,
            $LinkedinField,
            $TwitterField,
            $FacebookField,
            $InstagramField,
            $BlueskyField,
            $MastodonField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'FirstName'
        ));
    }

    public function validate()
    {
        $result = parent::validate();

        if(filter_var($this->owner->Linkedin, FILTER_VALIDATE_URL) == false && !empty($this->owner->Linkedin)) {
            $result->addError('Invalid link Linkedin');
        }
        if(filter_var($this->owner->Twitter, FILTER_VALIDATE_URL) == false && !empty($this->owner->Twitter)) {
            $result->addError('Invalid link Twitter');
        }
        if(filter_var($this->owner->Facebook, FILTER_VALIDATE_URL) == false && !empty($this->owner->Facebook)) {
            $result->addError('Invalid link Facebook');
        }
        if(filter_var($this->owner->Instagram, FILTER_VALIDATE_URL) == false && !empty($this->owner->Instagram)) {
            $result->addError('Invalid link Instagram');
        }
        if(filter_var($this->owner->Bluesky, FILTER_VALIDATE_URL) == false && !empty($this->owner->Bluesky)) {
            $result->addError('Invalid link Bluesky');
        }
        if(filter_var($this->owner->Mastodon, FILTER_VALIDATE_URL) == false && !empty($this->owner->Mastodon)) {
            $result->addError('Invalid link Mastodon');
        }

        return $result;
    }

    public function FullName(){

        return $this->FirstName." ".$this->Surname;

    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
