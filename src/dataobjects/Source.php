<?php

namespace Hestec\BlogExtensions;

use SilverStripe\ORM\DataObject;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Security\Permission;

class Source extends DataObject {

    private static $singular_name = 'Source';
    private static $plural_name = 'Sources';

    private static $table_name = 'HestecBlogExtensionsSource';

    private static $db = [
        'Name' => 'Varchar(255)',
        'Link' => 'Varchar(255)',
        'LinkType' => "Enum('FOLLOW,NOFOLLOW','FOLLOW')",
        'Sort' => 'Int'
    ];

    private static $has_one = [
        'BlogPost' => BlogPost::class
    ];

    private static $summary_fields = [
        'Name',
        'Link'
    ];

    public function getCMSFields() {
        //$fields = parent::getCMSFields();

        $NameField = TextField::create('Name', 'Name');
        $LinkField = TextField::create('Link', 'Link');
        $LinkTypeField = DropdownField::create('LinkType', 'LinkType', $this->dbObject('LinkType')->enumValues());


        return new FieldList(
            $NameField,
            $LinkField,
            $LinkTypeField
        );

    }

    public function getCMSValidator() {

        return new RequiredFields(array(
            'Name'
        ));
    }

    public function validate()
    {
        $result = parent::validate();

        if(filter_var($this->owner->Link, FILTER_VALIDATE_URL) == false && !empty($this->owner->Link)) {
            $result->addError('Invalid link');
        }

        return $result;
    }

    public function canView($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}