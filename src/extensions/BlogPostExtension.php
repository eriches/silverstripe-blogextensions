<?php

namespace Hestec\BlogExtensions;

use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\NumericField;

class BlogPostExtension extends DataExtension {

    private static $db = array(
        'ReadTime' => 'Int',
        'LastUpdated' => 'Datetime'
    );

    private static $has_one = array(
        'Author' => Author::class
    );

    private static $has_many = array(
        'Sources' => Source::class
    );

    public function updateCMSFields(FieldList $fields)
    {

        $SourcesGridField = GridField::create(
            'Sources',
            _t('BlogPost.SOURCES', "Sources"),
            $this->owner->Sources(),
            GridFieldConfig_RecordEditor::create()
                ->addComponent(new GridFieldOrderableRows())
        );

        $AuthorSource = $this->owner->Parent()->Authors()->map('ID', 'FullName');
        $AuthorField = DropdownField::create('AuthorID', _t('BlogPost.AUTHOR', "Author"), $AuthorSource);
        $AuthorField->setEmptyString("(select)");

        $ReadTimeField = NumericField::create('ReadTime', _t('BlogPost.READTIME', "Read time"));
        $ReadTimeField->setDescription(_t('BlogPost.IN_WHOLE_MINUTES', "In whole minutes"));

        $LastUpdatedField = DatetimeField::create('LastUpdated', _t('BlogPost.LASTUPDATED', "Last updated"));

        $fields->addFieldsToTab('Root.PostOptions', array(
            $ReadTimeField,
            $AuthorField,
            $SourcesGridField
        ));

        $fields->addFieldsToTab('Root.PostOptions', array(
            $LastUpdatedField
        ), 'Categories');

        return $fields;
    }

}
