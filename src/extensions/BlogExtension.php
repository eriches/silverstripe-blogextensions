<?php

namespace Hestec\BlogExtensions;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class BlogExtension extends DataExtension {

    private static $has_many = array(
        'Authors' => Author::class
    );

    public function updateCMSFields(FieldList $fields)
    {

        $AuthorsGridField = GridField::create(
            'Authors',
            'Authors',
            $this->owner->Authors(),
            GridFieldConfig_RecordEditor::create()
                //->addComponent(new GridFieldOrderableRows())
        );

        $fields->addFieldsToTab('Root.Authors', array(
            $AuthorsGridField
        ));

        return $fields;
    }

}
