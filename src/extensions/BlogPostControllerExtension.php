<?php

namespace Hestec\BlogExtensions;

use Spatie\SchemaOrg\Schema;
use SilverStripe\ORM\DataExtension;
use SilverStripe\SiteConfig\SiteConfig;

class BlogPostControllerExtension extends DataExtension {

    public function SchemaArticle()
    {

        $author_connections = array();
        if (filter_var($this->owner->Author()->Linkedin, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Linkedin);
        }
        if (filter_var($this->owner->Author()->Twitter, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Twitter);
        }
        if (filter_var($this->owner->Author()->Facebook, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Facebook);
        }
        if (filter_var($this->owner->Author()->Instagram, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Instagram);
        }
        if (filter_var($this->owner->Author()->Bluesky, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Bluesky);
        }
        if (filter_var($this->owner->Author()->Mastodon, FILTER_VALIDATE_URL)){
            array_push($author_connections, $this->owner->Author()->Mastodon);
        }

        $author = Schema::person();
        if ($this->owner->Author()->ID > 0){
            $author->givenName($this->owner->Author()->FirstName);
            $author->familyName($this->owner->Author()->Surname);
            $author->name($this->owner->Author()->FirstName.' '.$this->owner->Author()->Surname);
            if (!empty($author_connections)){
                $author->sameAs($author_connections);
            }
        }else{
            $siteconfig = SiteConfig::current_site_config();
            $author->givenName($siteconfig->DefaultAuthorFirstName);
            $author->familyName($siteconfig->DefaultAuthorSurname);
            $author->name($siteconfig->DefaultAuthorFirstName.' '.$siteconfig->DefaultAuthorSurname);
            $author->sameAs($siteconfig->DefaultAuthorSocial);
        }

        $image = Schema::imageObject();
        $image->url($this->owner->ShareImageLandscape()->AbsoluteURL);
        $image->width($this->owner->ShareImageLandscape()->Width);
        $image->height($this->owner->ShareImageLandscape()->Height);

        $article = Schema::article();
        $article->mainEntityOfPage($this->owner->SchemaWebpage());
        $article->image($image);
        $article->author($author);
        $article->publisher($this->owner->SchemaOrganization());
        $article->headline($this->owner->MetaTitle);
        $article->description($this->owner->MetaDescription);
        $article->datePublished($this->owner->PublishDate);
        if (isset ($this->owner->LastUpdated) && $this->owner->LastUpdated > $this->owner->PublishDate){
            $article->dateModified($this->owner->LastUpdated);
        }else{
            $article->dateModified($this->owner->PublishDate);
        }

        return $article;

    }

}
